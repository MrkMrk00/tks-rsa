#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define ERR_WRONG_ARGS 1
#define ERR_OUT_OF_MEM 2

#define ARG_MAX_LEN 32
#define FACTORS_BUF_SIZE 256

#define DEBUG

#ifdef DEBUG
#	define DEBUGF(...) printf("%s:%d :: \t", __FUNCTION__, __LINE__); \
	    printf(__VA_ARGS__)
#else
#	define DEBUGF(...)
#endif

typedef uint64_t 	u64;

static time_t TIME_START;

int strpos(const char *haystack, const char *needle)
{
    const char *p = strstr(haystack, needle);

    if (p) {
	return p - haystack;
    }

    return -1;
}

bool str_intlike(const char *szstr) 
{
    if (!szstr) {
	return false;
    }

    char ch;
    size_t i = 0;

    while ((ch = szstr[i++]) != '\0') {
	if (!isdigit(ch)) {
	    return false;
	}
    }

    return true;
}

bool is_prime_init(u64 number)
{
    if (number <= 2) {
	return true;
    }

    for (u64 i = 2; i < number; ++i) {
	if (number % i == 0) {
	    return false;
	}
    }

    return true;
}


typedef struct {
    char name[ARG_MAX_LEN + 1];
    char value[ARG_MAX_LEN + 1];
    bool valid;
} EqArg;

void EqArg_print(const EqArg *a)
{
    printf("{\n\t\"name\": \"%s\",\n\t\"value\": \"%s\",\n\t\"valid\": %d\n}\n",
	   a->name, a->value, a->valid);
}

EqArg parse_eq_arg(const char *szopt)
{
    EqArg o = (EqArg) {
	.name = { 0 },
	.value = { 0 },
	.valid = false,
    };

    size_t arglen = strlen(szopt);
    int eq_pos = strpos(szopt, "=");

    DEBUGF("arglen: %lu, eq_pos: %d\n", arglen, eq_pos);

    if (eq_pos < 1 || eq_pos >= (arglen - 1) || arglen > ARG_MAX_LEN * 2) {
	printf("Argument ignored: %s", szopt);

	return o;
    }

    // parse argument name
    int n = 0;
    char ch;

    while ((ch = szopt[n]) != '=' && n < ARG_MAX_LEN) {
	o.name[n++] = ch;
    }

    DEBUGF("Argument name: %s\n", o.name);

    int v = eq_pos + 1;

    while ((ch = szopt[v]) != '\0' && v < ARG_MAX_LEN) {
	o.value[v - (eq_pos + 1)] = ch;
	v++;
    }
    
    DEBUGF("Argument value: %s\n", o.value);
    
    o.valid = true;

    return o;
}

typedef struct {
    char command[17];
    ssize_t m, i, y;
    size_t prime_limit;
    bool log, verify, print_primes;
} ProgArgs;

void ProgArgs_print(const ProgArgs *c)
{
    printf(
	"{\n\t\"command\": \"%s\",\n\t\"i\": %zd,\n\t\"m\": %zd,\n\t\"y\": %zd,\n\t\"log\": %d,\n\t\"verify\": %d,\n\t\"limit\": %lu\n}\n",
	c->command,
	c->i,
	c->m,
	c->y,
	c->log,
	c->verify,
	c->prime_limit,
	c->print_primes
    );
}

ProgArgs parse_args(int argc, char *argv[])
{
    ProgArgs a = (ProgArgs) {
	.m = -1,
	.i = -1,
	.y = -1,
	.command = { 0 },
	.log = false,
	.verify = false,
	.prime_limit = 5000,
	.print_primes = false,
    };

    // argv[0] = program name
    const char *command = argv[1];
    size_t cmd_len = strlen(command);

    if (cmd_len < 1 || cmd_len > 16) {
	printf("Unknown command");

	exit(ERR_WRONG_ARGS);
    }

    strncpy(a.command, command, 16);

    for (int i = 2; i < argc; ++i) {
	char *arg = argv[i];
	size_t arglen = strlen(arg);
	int eq_pos = strpos(arg, "=");

	// eq_args: {{{
	if (eq_pos > 0) {
	    EqArg eq_arg = parse_eq_arg(argv[i]);
#ifdef DEBUG
	    EqArg_print(&eq_arg);
#endif
	    if (!eq_arg.valid) {
		DEBUGF("Argument ignored: %s", eq_arg.name);
		continue;
	    }

	    if (!str_intlike(eq_arg.value)) {
		DEBUGF("Argument ignored: %s", eq_arg.name);
		continue;
	    }

	    if (strcmp("m", eq_arg.name) == 0
		|| strcmp("n", eq_arg.name) == 0) {
		a.m = atoi(eq_arg.value);
	    } 

	    else if (strcmp("i", eq_arg.name) == 0) {
		a.i = atoi(eq_arg.value);
	    } 
	    
	    else if (strcmp("y", eq_arg.name) == 0) {
		a.y = atoi(eq_arg.value);
	    }

	    else if (strcmp("--prime-limit", eq_arg.name) ==0) {
		a.prime_limit = atoi(eq_arg.value);
	    }

	    continue;
	}
	// }}}
	
	if (strcmp(arg, "--log") == 0) {
	    a.log = true;
	}

	else if (strcmp(arg, "--verify") == 0) {
	    a.verify = true;
	}

	else if (strcmp(arg, "--print-primes") == 0) {
	    a.print_primes = true;
	}
    }

    return a;
}

static u64 *PRIME_CACHE 	= NULL;

void prime_cache_init(size_t limit)
{
    size_t alloc_count = (limit / log(limit)) + 0.1 * limit;

    DEBUGF("Allocating PRIME_CACHE with: limit=%lu; count=%lu; size=%.2f kB\n",
	   limit, alloc_count, (sizeof(u64) * alloc_count) / 1024.0);

    PRIME_CACHE = (u64*) malloc(sizeof(u64) * alloc_count);

    if (PRIME_CACHE == NULL) {
	fprintf(stderr, "Out of memmory!");
	exit(ERR_OUT_OF_MEM);
    }

    memset(PRIME_CACHE, 0, sizeof(u64) * alloc_count);

    size_t idx_cache = 0;

    for (u64 i = 2; i <= limit; ++i) {
	if (!is_prime_init(i)) {
	    continue;
	}
	
	if (idx_cache >= (alloc_count - 1)) {
	    size_t new_count = alloc_count * 1.2;
	    DEBUGF("Re-allocating PRIME_CACHE\nfrom: count=%lu; size=%lu\nto: count=%lu; size=%lu\n",
		alloc_count, sizeof(u64) * alloc_count, new_count, sizeof(u64) * new_count);

	    u64 *old_cache = PRIME_CACHE;

	    PRIME_CACHE = malloc(sizeof(u64) * new_count);
	    if (PRIME_CACHE == NULL) {
		fprintf(stderr, "Out of memmory!");
		exit(ERR_OUT_OF_MEM);
	    }

	    memset(PRIME_CACHE, 0, sizeof(u64) * new_count);
	    memcpy(PRIME_CACHE, old_cache, sizeof(u64) * alloc_count);

	    alloc_count = new_count;
	    free(old_cache);
	}


	PRIME_CACHE[idx_cache++] = i;
    }
}

void prime_cache_deinit()
{
    free(PRIME_CACHE);
}

int get_prime_factors(/** OUT */ u64 *factors, size_t i_max, u64 number)
{
    size_t idx_f = 0; 
    u64 prime;
    size_t pi = 0;

    while ((prime = PRIME_CACHE[pi++]) != 0) {
	if (number == prime) {
	    if (idx_f >= i_max) {
		DEBUGF("Buffer too small for prime factors!\n");
		return ERR_WRONG_ARGS;
	    }

	    factors[idx_f] = prime;
	    return 0;
	}

	if (number % prime == 0) {
	    DEBUGF("number: %lu; prime: %lu\n", number, prime);

	    if (idx_f >= i_max) {
		DEBUGF("Buffer too small for prime factors!\n");
		return ERR_WRONG_ARGS;
	    }

	    factors[idx_f++] = prime;
	    number = number / prime;
	    pi = 0;
	}
    }

    fprintf(stderr, "Couldn't find prime factors of number %lu\n", number);

    return ERR_WRONG_ARGS;
}

void ui_array_print(const u64 *arr, size_t i_max)
{
    u64 v;
    size_t i = 0;

    printf("[");
    while ((v = arr[i]) != 0 && i <= i_max) {
	if (i != 0) {
	    printf(", ");
	}

	printf("%lu", v);
	++i;
    }
    printf("]\n");
}

// ./solve primes n=%input%
int print_two_primes(const ProgArgs *a)
{
    if (a->m < 0) {
	fprintf(stderr, "Provide a value for prime number (./solve primes n=%%your number%%)\n");

	return ERR_WRONG_ARGS;
    }

    u64 buf[FACTORS_BUF_SIZE] = { 0 };
    int err = get_prime_factors(buf, FACTORS_BUF_SIZE - 1, a->m);

    if (err != 0) {
	return err;
    }

    ui_array_print(buf, FACTORS_BUF_SIZE - 1);

    size_t i = 0;
    u64 factor;
    
    return 0;
}

typedef struct {
    u64 factor;
    unsigned int count; 
} FactorCount;

int print_rsa_decrypt(const ProgArgs *a)
{
    if (a->m < 3 || a->i < 1 || a->y < 1) {
	fprintf(stderr, "Provide valid arguments!\n");
	return ERR_WRONG_ARGS;
    }

    u64 two_primes[2] = { 0 };
    int err = get_prime_factors(two_primes, 2, a->m);

    if (err || two_primes[0] == 0 || two_primes[1] == 0) {
	fprintf(stderr, "Could not find 2 factors for input number m=%zd\n", a->m);
	return err;
    }

    u64 p, q;
    p = two_primes[0];
    q = two_primes[1];

    u64 phi_base_num = (p - 1) * (q - 1);
    if (a->log) {
	printf("m (modul)=%lu\np=%lu\nq=%lu\nphi_base_num=%lu\n",
	    a->m, p, q, phi_base_num);
    }

    u64 phi_factors[FACTORS_BUF_SIZE] = { 0 };
    err = get_prime_factors(phi_factors, FACTORS_BUF_SIZE - 1, phi_base_num);

    if (err != 0) {
	fprintf(stderr, "Could not get prime factors of phi.\n");
	return err;
    }

    if (a->log) {
	printf("factors=");
	ui_array_print(phi_factors, FACTORS_BUF_SIZE - 1);
    }

    FactorCount counts[FACTORS_BUF_SIZE] = { 0 };
    int idx_c = 0;
    bool is_initd = false;

    int idx_f = 0;
    u64 last_factor = phi_factors[0];
    u64 factor;

    while ((factor = phi_factors[idx_f++]) != 0) {
	if (factor != last_factor) {
	    is_initd = false;
	    ++idx_c;
	}

	if (!is_initd) {
	    counts[idx_c] = (FactorCount) {
		.factor = factor,
		.count = 0,
	    };

	    is_initd = true;
	}

	counts[idx_c].count += 1;
    }

    if (a->log) {
	printf("counts: { ");
	for (int i = 0; i <= idx_c; ++i) {
	    if (i != 0) {
		printf(", ");
	    }

	    FactorCount fc = counts[i];
	    printf("%lu:%d", fc.factor, fc.count);
	}

	printf(" }\n");
    }

    u64 phi = 1;
    for (int i = 0; i <= idx_c; ++i) {
	FactorCount fc = counts[i];
	int exponent = fc.count;
	u64 factor = fc.factor;

	phi *= (factor - 1) * pow(factor, exponent - 1);
    }

    // running squares
    u64 j = 1;
    u64 phi_exponent = phi - 1;
    u64 last_result = 0;
    for (int exp = 0; exp <= floor(log2(phi_exponent)); ++exp) {
	if (last_result == 0) {
	    last_result = (u64) pow(a->i, pow(2, exp)) % phi_base_num;
	} else {
	    last_result = (u64) pow(last_result, 2) % phi_base_num;
	}

	j *= last_result;
    }

    //nefunguje
    j = j % phi_base_num;

    if (a->log) {
	printf("phi: %lu\ni (public key)=%ld\ny (cipher text)=%ld\nj (secret key)=%ld\n", 
	phi, a->i, a->y, j);
    }



    return 0;
}

int main(int argc, char *argv[]) 
{
    if (argc < 2) {
	printf("Provide command line arguments!\n");

	return ERR_WRONG_ARGS;
    }

    int err_code = 0;
    const ProgArgs a = parse_args(argc, argv);

#ifdef DEBUG
    ProgArgs_print(&a);
#endif
    prime_cache_init(a.prime_limit);

    if (a.print_primes) {
	size_t i = 0;
	u64 prime;
	while ((prime = PRIME_CACHE[i]) != 0) {
	    if (i % 10 == 0) {
		printf("\n");
	    }

	    printf("(%lu: %lu) ", i, prime);
	    ++i;
	}
	printf("\n");
	printf("Prime count: %lu\n", i);
    }

    if (strcmp(a.command, "primes") == 0) {
	err_code = print_two_primes(&a);
    } 

    else if (strcmp(a.command, "rsa") == 0) {
	err_code = print_rsa_decrypt(&a);
    }

    prime_cache_deinit();

    return err_code;
}

