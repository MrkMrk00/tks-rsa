#!/usr/bin/env python

import sys
from typing import Tuple, Any

PRIME_CACHE = list()

class MetaArgs:
    def __init__(self, args: list[str]):
        self._args = args

    @property
    def log(self) -> bool:
        return '--log' in self._args

    @property
    def verify(self) -> bool:
        return '--verify' in self._args

    @property
    def prime_limit(self) -> int:
        limit = 5000
        for arg in self._args:
            if arg.startswith('--prime-limit='):
                _, str_limit = arg.split('=')
                limit = int(str_limit)

        return limit

    def __contains__(self, item):
        return item in self._args


def DEBUG(*args):
    if META_ARGS.log:
        print(*args)


META_ARGS: MetaArgs

def is_prime_init(num: int) -> bool:
    for i in range(2, num):
        if num % i == 0:
            return False

    return True


def is_prime(num: int) -> bool:
    """
    Check whether num is prime using PRIME_CACHE
    """

    return num in PRIME_CACHE if num <= META_ARGS.prime_limit else is_prime_init(num)


def get_two_primes(num: int) -> Tuple[int, int] | None:
    for i in range(2, num):
        if not is_prime(i):
            continue

        if num % i == 0 and is_prime(num // i):
            return (num // i, i)

    return None


def get_factors(num: int, _acc: list[int] = list()) -> list[int]:
    for p in PRIME_CACHE:
        if p == num:
            return [*_acc, num]
        
        if num % p == 0:
            return get_factors(num // p, [*_acc, p])
    
    raise Exception('Could not find factors')


def get_num_counts(nums: list[int]) -> dict[int, int]:
    counts = dict()
    for n in nums:
        if n not in counts:
            counts[n] = 0

        counts[n] += 1

    return counts


def rsa_encrypt(x: int, i: int, m: int) -> int:
    """
    x = message
    i = public key
    m = modul
    """

    return (x ** i) % m

    
def rsa_decrypt(args: dict[str, Any]):
    m = int(args['n'] or args['m'])
    DEBUG(f'm (modul)={m}')

    primes = get_two_primes(m)
    if primes is None:
        raise Exception('Cannot find 2 primes for {}'.format(m))

    (p, q) = primes
    phi_base_num = (p - 1) * (q - 1)
    DEBUG(f'{p=}\n{q=}\n{phi_base_num=}')
    
    factors = get_factors(phi_base_num)
    counts = get_num_counts(factors)
    DEBUG(f'{factors=}\n{counts=}')

    phi = 1
    for factor, exponent in counts.items():
        phi *= (factor - 1) * (factor ** (exponent - 1))

    DEBUG(f'{phi=}')

    i = int(args['i'])
    DEBUG(f'public key (i)={i}')

    j = (i ** (phi - 1)) % phi_base_num
    DEBUG(f'j (secret exponent)={j}')

    y = int(args['y'])
    DEBUG(f'y (cipher text)={y}')

    x = y ** j % m

    print(f'x (message)={x}')

    if META_ARGS.verify:
        verified_result = rsa_encrypt(x=x, i=i, m=m)
        print(f'y (verified)={verified_result}')

        matches = verified_result == y
        print(f'{matches=}')


def main(cmd: str|None, args: list[str], named_args: dict[str, Any]):
    if cmd == 'prime':
        print(get_two_primes(int(args[0])))
        return

    if cmd == 'rsa':
        rsa_decrypt(named_args)
        return

    raise Exception('Unknown command')


if __name__ == '__main__':
    META_ARGS = MetaArgs([ x for x in sys.argv if '-' in x ])

    for i in range(2, META_ARGS.prime_limit):
        if is_prime_init(i):
            PRIME_CACHE.append(i)

    all_args = [ x for x in sys.argv if x not in META_ARGS and '.py' not in x ]

    eq_args = [ x for x in all_args if '=' in x ]
    cmd = [ x for x in all_args if x not in eq_args ]
    
    named_args = { k:v for [k, v] in [ x.split('=') for x in eq_args ] }
    main(cmd[0].lower() if len(cmd) > 0 else None, cmd[1:], named_args)

